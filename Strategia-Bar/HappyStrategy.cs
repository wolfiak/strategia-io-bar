﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia_Bar
{
    class HappyStrategy : IBillingStrategy
    {
        public double getPrice(double rawPrice)
        {
            return rawPrice * 0.5;
        }
    }
}
