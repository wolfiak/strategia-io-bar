﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia_Bar
{
    class Program
    {
        static void Main(string[] args)
        {
            Klient uno = new Klient(new NormalStrategy());
            uno.add(2.0, 5);
            uno.strategia = new HappyStrategy();
            uno.add(2.0, 5);
            uno.printBill();
            Console.WriteLine("Koniec");
        }
    }
}
