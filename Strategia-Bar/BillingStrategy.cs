﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia_Bar
{
    interface IBillingStrategy
    {
         double getPrice(double rawPrice);

    }
}
