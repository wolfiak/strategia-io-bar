﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategia_Bar
{
    class Klient
    {
        public IBillingStrategy strategia { get; set;}
        public List<Double> drinks { get; set;}
        public Klient(IBillingStrategy strategia)
        {
            this.strategia = strategia;
            drinks = new List<double>();
        }
        public void add(double price, int ilosc)
        {
            drinks.Add(strategia.getPrice(price * ilosc));
        }
        public void printBill()
        {
            double suma = 0;
            foreach(double n in drinks)
            {
                suma += n;
            }
            Console.WriteLine("TOTAL:" + suma);
            drinks.Clear();
        }

    }
}
